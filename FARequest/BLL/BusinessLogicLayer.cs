﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Reflection;

namespace FARequest.BLL
{
    public class BusinessLogicLayer
    {
        public List<HttpPostedFileBase> ValidateFiles(List<HttpPostedFileBase> postedFiles)
        {
            List<HttpPostedFileBase> validFiles = new List<HttpPostedFileBase>();

            foreach (HttpPostedFileBase file in postedFiles)
            {
                if (file != null)
                {
                    string ext = file.FileName.Substring(file.FileName.LastIndexOf('.'));

                    if ((ext == ".pdf") || (ext == ".jpg"))
                    {
                        if (file.ContentLength <= 3000000)
                        {
                            validFiles.Add(file);
                        }
                    }
                }
            }

            return validFiles;
        }
    }
}