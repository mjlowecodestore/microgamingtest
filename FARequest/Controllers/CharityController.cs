﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FARequest.Models;
using System.Data;
using System.Data.SqlClient;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using System.IO;
using FARequest.BLL;
using FARequest.DAL;


namespace FARequest.Controllers
{
    public class CharityController : Controller
    {
        private string _userId;
        private string _userRole;
        
        // GET: Requests
        [Authorize(Roles = "Admin, Employee")]
        public ActionResult Index()
        {
            var userId = User.Identity.GetUserId();
            _userId = userId;

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            _userRole = roles[0].Value.ToString();


            DBHelper dbh = new DBHelper();
            ModelState.Clear();

            var model = new List<CharityRequestModel>();
            model = model = dbh.GetRequests(_userId, _userRole);
          
            return View(model);
            //return View(dbh.GetRequests(_userId, _userRole));
        }

        [Authorize(Roles = "Admin, Employee")]
        // GET: charity/Edit/5
        public ActionResult Edit(int id)
        {
            DBHelper dbh = new DBHelper();

            var userId = User.Identity.GetUserId();
            _userId = userId;

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            _userRole = roles[0].Value.ToString();

            var model = new CharityRequestModel();

            if (User.IsInRole("Admin"))
            {
                model = dbh.GetRequests(_userId, _userRole).Find(RequestModel => RequestModel.RequestId == id);
                return View("AdminEdit", model);
            }
            else
            {
                model = dbh.GetRequests(_userId, _userRole).Find(RequestModel => RequestModel.RequestId == id);
                return View("Edit", model);
            }
        }

        // POST: charity/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, CharityRequestModel Requestmodel)
        {
            DBHelper dbh = new DBHelper();

            if (User.IsInRole("Admin"))
            {
                try
                {
                    dbh.UpdateRequestStatus(Requestmodel);
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    string exception = "Admin Update Error" + " " + ex.ToString();
                    return View();
                }
            }
            else
            {
                try
                {
                    dbh.UpdateDetails(Requestmodel);
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    string exception = "Employee Update Error" + " " + ex.ToString();
                    return View();
                }
            }
        }


        [Authorize(Roles = "Admin, Employee")]
        // GET: charity/Create
        public ActionResult CreateDocuments()
        {
            return View();
        }

        [Authorize(Roles = "Admin, Employee")]
        // POST: charity/Create
        [HttpPost]
        public ActionResult CreateDocuments(CharityRequestModel Requestmodel, List<HttpPostedFileBase> postedFiles)
        {
            BLL.BusinessLogicLayer bll = new BusinessLogicLayer();

            var userId = User.Identity.GetUserId();
            _userId = userId;

            Requestmodel.EmployeeId = _userId;
            Requestmodel.DateSubmitted = DateTime.UtcNow;
            Requestmodel.DateModified = DateTime.UtcNow;
            Requestmodel.StatusDesc = "New";
            Requestmodel.StatusId = 1;

            bool SaveFileSuccess = false;

            if (postedFiles.Count > 1)
            {
                List<HttpPostedFileBase> allValidFiles = bll.ValidateFiles(postedFiles);
                ViewData["ApprovedFileList"] = allValidFiles;

                bool ValidTotalSize = CheckFilesTotalSize(allValidFiles);

                if (ValidTotalSize)
                {
                    SaveFileSuccess = UploadValidFiles(allValidFiles, Requestmodel);
                }

                if (SaveFileSuccess)
                {
                    SaveData(Requestmodel);
                }
            }
            else
            {
                SaveData(Requestmodel);
            }

            return View();
        }

        [Authorize(Roles = "Admin, Employee")]
        public bool SaveData(CharityRequestModel Requestmodel)
        {
            DBHelper dbh = new DBHelper();

            if (dbh.AddRequest(Requestmodel))
            {
                ViewBag.Message = "Request Details Added Successfully";
                ModelState.Clear();
                return true;
            }
            else
            {
                ViewBag.Message = "Request Details Failed";
                return false;
            }
        }

        public bool CheckFilesTotalSize(List<HttpPostedFileBase> allValidFiles)
        {
            int sumOfLength = 0;

            for (int i = 0; i < allValidFiles.Count; i++)
            {
                sumOfLength += allValidFiles[i].ContentLength;
            }

            if (sumOfLength > 15000000)
            {
                ViewData["FileSizeError"] = sumOfLength + "This cannot exceed 15MB.";
                return false;
            }
            return true;
        }

        public bool UploadValidFiles(List<HttpPostedFileBase> allValidFiles, CharityRequestModel RequestModel)
        {
            try
            {
                foreach (HttpPostedFileBase file in allValidFiles)
                {
                    if (ModelState.IsValid)
                    {
                        string path = Server.MapPath("~/Uploads/" + _userId + "/" + RequestModel.Charity + "/");
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }

                        RequestModel.DocPath = path;

                        var InputFileName = Path.GetFileName(file.FileName);
                        var ServerSavePath = Path.Combine(path + InputFileName);

                        file.SaveAs(ServerSavePath);

                        ViewBag.UploadStatus = allValidFiles.Count().ToString() + " files uploaded successfully.";

                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                string err = ex.ToString();
                return false;
            }
            return false;
        }

        [Authorize(Roles = "Admin, Employee")]
        // GET: charity/Delete/5
        public ActionResult Delete(int id)
        {
            var userId = User.Identity.GetUserId();
            _userId = userId;

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            _userRole = roles[0].Value.ToString();

            DBHelper dbh = new DBHelper();
            var model = new CharityRequestModel();

            if (User.IsInRole("Admin"))
            {
                model = dbh.GetRequests(_userId, _userRole).Find(RequestModel => RequestModel.RequestId == id);
                return View("AdminDelete", model);
            }
            else
            {
                model = dbh.GetRequests(_userId, _userRole).Find(RequestModel => RequestModel.RequestId == id);
                return View("EmployeeDelete", model);
            }
        }

        [Authorize(Roles = "Admin, Employee")]
        // POST: charity/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, CharityRequestModel Requestmodel)
        {
            DBHelper dbh = new DBHelper();

            if (User.IsInRole("Admin"))
            {
                try
                {
                    dbh.DeleteRequest(id);
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    string exception = "Admin Delete Error" + " " + ex.ToString();
                    ViewData["StatusDeleteError"] = "Admin Delete Error";
                    return View();
                }
            }
            else
            {
                try
                {
                    dbh.DeleteRequest(id);
                    return RedirectToAction("Index");

                }
                catch (Exception ex)
                {
                    string exception = "Employee Delete Error" + " " + ex.ToString();
                    ViewData["StatusDeleteError"] = "Employee Delete Error";
                    return View();
                }
            }
        }
    }
}
