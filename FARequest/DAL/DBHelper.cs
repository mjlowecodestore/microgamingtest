﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Mvc;
using FARequest.Models;

namespace FARequest.DAL
{
    public class DBHelper
    {
        private SqlConnection con;
        private void connection()
        {
            string constring = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            con = new SqlConnection(constring);
        }


        public List<SelectListItem> GetTESTRequestStatuses()
        {
            connection();
            SqlCommand cmd = null;
            CharityRequestModel crm = new CharityRequestModel();

            cmd = new SqlCommand("sp_Get_RequestsStatus", con);

            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }

            sd.Fill(dt);
            con.Close();

            List<SelectListItem> StatusList = new List<SelectListItem>();

            foreach (DataRow item in dt.Rows)
            {
                string Text = item["Description"].ToString();
                int Value = Convert.ToInt32(item["StatusId"].ToString());

                StatusList.Add(new SelectListItem {Text = item["Description"].ToString(), Value = item["StatusId"].ToString()});
            }

            crm.TestStatusTypes = StatusList;
            crm.StatusId = 1;

            return StatusList;
        }

        //Method to get all Requests
        public List<CharityRequestModel> GetRequests(string UserId, string Role)
        {
            connection();
            SqlCommand cmd = null;
            List<CharityRequestModel> RequestList = new List<CharityRequestModel>();

            cmd = new SqlCommand("sp_Get_CharityRequest", con);
            cmd.Parameters.AddWithValue("@EmpId", UserId);
            cmd.Parameters.AddWithValue("@Role", Role);

            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }

            sd.Fill(dt);

            con.Close();

            List<SelectListItem> test = new List<SelectListItem>();
            test = GetTESTRequestStatuses();


            foreach (DataRow dr in dt.Rows)
            {
                RequestList.Add(
                    new CharityRequestModel
                    {
                        Title = Convert.ToString(dr["Title"]),
                        Charity = Convert.ToString(dr["Charity"]),
                        Description = Convert.ToString(dr["Description"]),
                        Amount = Convert.ToDecimal(dr["Amount"]),
                        StatusDesc = Convert.ToString(dr["StatusDesc"]),
                        DateModified = Convert.ToDateTime(dr["DateModified"]),
                        DateSubmitted = Convert.ToDateTime(dr["DateCreated"]),
                        DocPath = Convert.ToString(dr["DocPath"]),
                        PreviouslySupported = Convert.ToBoolean(dr["PreviousSupport"]),
                        RequestId = Convert.ToInt32(dr["CharityRequestId"]),
                        StatusId = Convert.ToInt32(dr["StatusId"]),
                        TestStatusTypes = test

                    });
            }





            return RequestList;
        }

        // **************** ADD NEW Request *********************
        public bool AddRequest(CharityRequestModel Requestmodel)
        {

            connection();
            SqlCommand cmd = new SqlCommand("sp_Insert_CharityRequest", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@Title", Requestmodel.Title);
            cmd.Parameters.AddWithValue("@Description", Requestmodel.Description);
            cmd.Parameters.AddWithValue("@Charity", Requestmodel.Charity);
            cmd.Parameters.AddWithValue("@Amount", Requestmodel.Amount);
            cmd.Parameters.AddWithValue("@DateModified", Requestmodel.DateModified);
            cmd.Parameters.AddWithValue("@DateCreated", Requestmodel.DateSubmitted);
            cmd.Parameters.AddWithValue("@PreviousSupport", Requestmodel.PreviouslySupported);
            cmd.Parameters.AddWithValue("@DocPath", Requestmodel.DocPath);
            cmd.Parameters.AddWithValue("@EmpId", Requestmodel.EmployeeId);
            cmd.Parameters.AddWithValue("@StatusId", Requestmodel.StatusId);

            try
            {
                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();

                if (i >= 1)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {

                throw ex ;
            }
        }


        // **************** ADD NEW Request *********************
        public bool AddUserRole(string UserId)
        {
            connection();
            SqlCommand cmd = new SqlCommand("sp_Insert_UserEmployeeRole", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@UserId", UserId);
            cmd.Parameters.AddWithValue("@RoleId", 2);

            try
            {
                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();

                if (i >= 1)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        // ***************** UPDATE REQUEST DETAILS *********************
        public bool UpdateDetails(CharityRequestModel Requestmodel)
        {
            connection();
            SqlCommand cmd = new SqlCommand("sp_Update_CharityRequest_By_CharityRequestId", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@CharityRequestId", Requestmodel.RequestId);
            cmd.Parameters.AddWithValue("@Title", Requestmodel.Title);
            cmd.Parameters.AddWithValue("@Description", Requestmodel.Description);
            cmd.Parameters.AddWithValue("@Charity", Requestmodel.Charity);
            cmd.Parameters.AddWithValue("@PreviousSupport", Requestmodel.PreviouslySupported);
            cmd.Parameters.AddWithValue("@Amount", Requestmodel.Amount);
            cmd.Parameters.AddWithValue("@DateModified", DateTime.UtcNow);
            cmd.Parameters.AddWithValue("@StatusId", Requestmodel.StatusId);


            try
            {
                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();

                if (i >= 1)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {

                throw ex;
            }

 
        }


        public bool UpdateRequestStatus(CharityRequestModel Requestmodel)
        {
            connection();
            SqlCommand cmd = new SqlCommand("sp_Update_RequestStatus_By_RequestId", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@CharityRequestId", Requestmodel.RequestId);
            cmd.Parameters.AddWithValue("@DateModified", DateTime.UtcNow);
            cmd.Parameters.AddWithValue("@StatusId", Requestmodel.StatusId);

            try
            {
                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();

                if (i >= 1)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {

                throw ex;
            }


        }

        public bool DeleteRequest(int id)
        {
            connection();
            SqlCommand cmd = new SqlCommand("sp_Delete_CharityRequest_By_CharityRequestId", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@CharityRequestId", id);
            try
            {
                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();

                if (i >= 1)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {

                throw ex;
            }


        }

    }
}