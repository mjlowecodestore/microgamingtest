﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Data;

namespace FARequest.Models
{
    public class CharityRequestModel
    {
        public int RequestId { get; set; }

        [Display(Name = "Title")]
        [Required(ErrorMessage = "Title is required.")]
        public string Title { get; set; }

        [Display(Name = "Description")]
        [Required(ErrorMessage = "Description is required.")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Display(Name = "Charity")]
        [Required(ErrorMessage = "Charity is required.")]
        [DataType(DataType.MultilineText)]
        public string Charity { get; set; }
        public bool PreviouslySupported { get; set; }
        public decimal Amount { get; set; }
        public string DocPath { get; set; }
        public string StatusDesc { get; set; }
        public DateTime DateSubmitted { get; set; }
        public DateTime DateModified { get; set; }
        public string EmployeeId { get; set; }
        [Key]
        public int StatusId { get; set; }
        public List<SelectListItem> TestStatusTypes { get; set; }


    }
}